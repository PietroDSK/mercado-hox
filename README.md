<h1>Como usar:</h1>


1. Entre com o Prompt de comando na pasta do programa. 
2. Utilize o comando **npm install** para instalar os *node.modules* 
3. Rode o Json-server com o comando **json-server --watch db.json**
4. Rode o aplicativo com o comando **npm start**
5. Quando o prompt pedir, digite **Y** e de **ENTER** para iniciar o app em localhost:3001
5. Pronto agora é só usar!

<h2>OBS: Rode primeiro o Json-Sever</h2>