import React from 'react';

const ItemLista = props => (
    <tr>
        <td>{props.nome}</td>
        <td>{props.fabricado}</td>
        <td>{props.perecivel}</td>
        <td>{props.validade}</td>
        <td>{props.preco}</td>    
        </tr>
)

export default ItemLista