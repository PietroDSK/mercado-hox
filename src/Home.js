import React, { Component } from 'react'
import axios from 'axios'
import ItemLista from './ItemLista'

import './tabela.css'
export default class Home extends Component {
   
   state={
       produtos: []
   }

   componentDidMount(){
       axios.get('http://localhost:3000/produtos')
       .then(res => {
           const produtos = res.data
           this.setState({ produtos })
       })
    
   }
    render() {
        return (
            <div className="content">
               <table className="rTable">
               <thead>
                   <tr>
                    <th>Nome</th>
                    <th>Data de Fabricação</th>
                    <th>Perecivel</th>
                    <th>Prazo de Validade</th>
                    <th>Preço</th>
                    </tr>
                   </thead>
                   <tbody>
                  {this.state.produtos.map((produto) => (
                      <ItemLista key= {produto.id} nome={produto.nome} fabricado={produto.fabricado} perecivel={produto.perecivel} validade={produto.validade} preco={produto.preco}/>
                  ))}
                </tbody>
               </table>
         
          
            </div>
        )
    }
}
